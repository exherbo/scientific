# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require alternatives

export_exlib_phases src_configure src_compile src_test src_install

SUMMARY="A library for handling files in the Flexible Image Transport System format"
DESCRIPTION="
CFITSIO provides simple high-level routines for reading and writing FITS files
that insulate the programmer from the internal complexities of the FITS format.
CFITSIO also provides many advanced features for manipulating and filtering
the information in FITS files.
The Flexible Image Transport System is an open standard defining a digital
file format useful for storage, transmission and processing of scientific and
other images. FITS is the most commonly used digital file format in astronomy.
Unlike many image formats, FITS is designed specifically for scientific data
and hence includes many provisions for describing photometric and spatial
calibration information, together with image origin metadata.
"

HOMEPAGE="https://heasarc.gsfc.nasa.gov/fitsio/"
DOWNLOADS="https://heasarc.gsfc.nasa.gov/FTP/software/fitsio/c/${PNV}.tar.gz"

UPSTREAM_DOCUMENTATION="https://heasarc.gsfc.nasa.gov/docs/software/fitsio/c/c_user/cfitsio.html"
UPSTREAM_RELEASE_NOTES="https://heasarc.gsfc.nasa.gov/FTP/software/fitsio/c/docs/changes.txt"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="
    doc

    (
        platform: amd64 x86
        amd64_cpu_features: ssse3
        x86_cpu_features: ssse3
    )
"

DEPENDENCIES="
    build+run:
        app-arch/bzip2
        net-misc/curl
        sys-libs/zlib
    run:
        !sci-libs/cfitsio:0[<3.450-r2] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( docs/changes.txt )
DEFAULT_SRC_INSTALL_EXCLUDE=( README.MacOS README_OLD.win README.win )

cfitsio_src_configure() {
    local config_params=(
        --enable-reentrant
        --disable-hera
        --with-bzip2=/usr/$(exhost --target)
        --without-gsiftp
        --without-gsiftp-flavour
    )

    if option platform:amd64 ; then
        config_params+=(
            --enable-sse2
            $(option_enable amd64_cpu_features:ssse3)
        )
    elif option platform:x86 ; then
        config_params+=(
            $(option_enable x86_cpu_features:sse2)
            $(option_enable x86_cpu_features:ssse3)
        )
    fi

    econf "${config_params[@]}"
}

cfitsio_src_compile() {
    emake shared

    emake fpack
    emake funpack

    expecting_tests && emake testprog
}


cfitsio_src_test() {
    # README
    LD_LIBRARY_PATH=. edo ./testprog > testprog.lis
    edo diff testprog.lis testprog.out
    edo cmp testprog.fit testprog.std
}

cfitsio_src_install() {
    local alternatives=() host=$(exhost --target)

    default

    alternatives+=(
        /usr/${host}/bin/fpack              fpack-${SLOT}
        /usr/${host}/bin/funpack            funpack-${SLOT}
        /usr/${host}/include/drvrsmem.h     drvrsmem-${SLOT}.h
        /usr/${host}/include/fitsio2.h      fitsio2-${SLOT}.h
        /usr/${host}/include/fitsio.h       fitsio-${SLOT}.h
        /usr/${host}/include/longnam.h      longnam-${SLOT}.h
        /usr/${host}/lib/lib${PN}.a        lib${PN}-${SLOT}.a
        /usr/${host}/lib/lib${PN}.so        lib${PN}-${SLOT}.so
        /usr/${host}/lib/pkgconfig/${PN}.pc ${PN}-${SLOT}.pc
    )

    option doc && dodoc docs/{cfitsio,fitsio,fpackguide,quick}.pdf

    expecting_tests && edo rm "${IMAGE}"/usr/$(exhost --target)/bin/testprog

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${alternatives[@]}"
}

