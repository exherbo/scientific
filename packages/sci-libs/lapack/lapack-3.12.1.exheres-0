# Copyright 2010 Elias Pipping <pipping@exherbo.org>
# Copyright 2010 Cecil Curry <leycec@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user=Reference-${PN} tag=v${PV} ] cmake alternatives

SUMMARY="Linear Algebra Package"
HOMEPAGE+=" https://www.netlib.org/${PN}"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    lapacke [[ description = [ Enable C wrapper ] ]]
"

# tests are extremely resource hungry and slow, use src_test_expensive
RESTRICT="test"

DEPENDENCIES="
    build+run:
        sys-libs/libgfortran:=
        virtual/blas
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBLAS++:BOOL=FALSE
    # link to libblas.so for alternatives handling
    -DBLAS_LIBRARIES:STRING=blas
    -DBUILD_COMPLEX:BOOL=TRUE
    -DBUILD_COMPLEX16:BOOL=TRUE
    -DBUILD_DEPRECATED:BOOL=FALSE
    -DBUILD_DOUBLE:BOOL=TRUE
    -DBUILD_INDEX64:BOOL=FALSE
    -DBUILD_INDEX64_EXT_API:BOOL=FALSE
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DBUILD_SINGLE:BOOL=TRUE
    -DCMAKE_Fortran_COMPILER=${FORTRAN}
    -DLAPACK++:BOOL=FALSE
    -DLAPACK_TESTING_USE_PYTHON:BOOL=FALSE
    -DTEST_FORTRAN_COMPILER:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_OPTIONS=(
    'lapacke LAPACKE'
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
)

src_install() {
    cmake_src_install

    # Do not install .cmake files
    edo rm -rf "${IMAGE}"/usr/$(exhost --target)/lib/cmake

    # Name of LAPACK library to be installed.
    LAPACK_LIBRARY="liblapack-netlib.so"
    edo mv "${IMAGE}"/usr/$(exhost --target)/lib/{liblapack.so,${LAPACK_LIBRARY}}
    alternatives_for lapack lapack-netlib \
        0 /usr/$(exhost --target)/lib/liblapack.so ${LAPACK_LIBRARY}

    # dirty hack to prevent e.g. scipy from failing to link lapack until our
    # whole blas/openblas/lapack alternatives mess has been sorted out properly.
    if [[ $(eclectic blas show) == OpenBLAS ]]; then
        edo sed \
            -e 's:blas:openblas:g' \
            -i "${IMAGE}"/usr/$(exhost --target)/lib/pkgconfig/lapack.pc
    fi
}

src_test_expensive() {
    ectest
}

