# Copyright 2007 Danny van Dyk
# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require gnu [ suffix=tar.xz ] \
    freedesktop-desktop \
    gtk-icon-cache

export_exlib_phases src_prepare src_configure src_install pkg_postinst pkg_postrm

SUMMARY="GNU Octave is a high-level language, primarily intended for numerical computations"
DESCRIPTION="
GNU Octave is a high-level language, primarily intended for numerical
computations. It provides a convenient command line interface for solving
linear and nonlinear problems numerically, and for performing other numerical
experiments using a language that is mostly compatible with Matlab. It may also
be used as a batch-oriented language.
"

UPSTREAM_DOCUMENTATION="
https://www.gnu.org/software/octave/about.html       [[ lang = en description = [ A short introduction to GNU Octave ] ]]
https://www.gnu.org/software/octave/doc/interpreter  [[ lang = en description = [ The GNU Octave reference manual, HTML ] ]]
https://www.gnu.org/software/octave/FAQ.html         [[ lang = en description = FAQ ]]
"
UPSTREAM_RELEASE_NOTES="https://www.gnu.org/software/${PN}/NEWS-$(ever range 1-2).html"

LICENCES="GPL-3"
SLOT="0"
# TODO readd providers: imagemagick
# once upstream actually cares: https://savannah.gnu.org/bugs/index.php?63331
MYOPTIONS="
    curl
    doc
    gui [[
        description = [ Build a Qt5 based GUI ]
        requires = [ opengl ]
    ]]
    opengl [[ description = [ native graphics ] ]]
    openmp
    portaudio [[
        description = [ Support audio recording/playback (audioplayer, audiorecorder and
                        audiodevinfo function) using PortAudio ]
    ]]
    sndfile [[
        description = [ Support audio processing (audioinfo, audioread and audiowrite
                        function) using libsndfile ]
    ]]

    ( providers: graphicsmagick ) [[ number-selected = at-most-one ]]
"

# providers:imagemagick? ( media-gfx/ImageMagick )
DEPENDENCIES="
    build:
        dev-util/gperf
        sys-devel/bison
        sys-devel/flex
        virtual/pkg-config
    build+run:
        app-arch/bzip2
        dev-libs/pcre2
        sci-apps/gnuplot
        sci-libs/SuiteSparse
        sci-libs/arpack
        sci-libs/fftw
        sci-libs/glpk
        sci-libs/hdf5
        sci-libs/qrupdate
        sys-libs/libgfortran:*
        sys-libs/zlib
        virtual/blas
        virtual/lapack
        curl? ( net-misc/curl )
        doc? (
            app-text/dvipsk
            app-text/texlive-core
            sys-apps/texinfo
        )
        gui? (
            app-editors/QScintilla[>=2.10] [[ note = [ First version using Qt5 ] ]]
            x11-libs/qtbase:5[gui]
            x11-libs/qttools:5[gui] [[ note = [ qcollectiongenerator ] ]]
        )
        opengl? (
            dev-libs/libglvnd
            media-libs/fontconfig
            media-libs/freetype:2
            x11-dri/glu
            x11-libs/fltk
            x11-libs/libX11
            x11-libs/libXft
        )
        openmp? ( sys-libs/libgomp:* )
        portaudio? ( media-libs/portaudio )
        providers:graphicsmagick? ( media-gfx/GraphicsMagick )
        sndfile? ( media-libs/libsndfile )
    suggestion:
        opengl? ( x11-libs/gl2ps )
"

octave_src_prepare() {
    edo sed \
        -e 's|%OCTAVE_PREFIX%/bin/||' \
        -i etc/icons/org.octave.Octave.desktop.in

    default
}

octave_src_configure() {
    local myconf=(
        --enable-command-line-push-parser
        --enable-vm-evaluator
        --enable-year2038
        --disable-java
        --disable-lib-visibility-flags
        --disable-rapidjson
        --disable-relocate-all
        --disable-std-pmr-polymorphic-allocator
        --with-fftw3
        --with-glpk
        --with-hdf5
        --with-pcre2
        --without-pcre

        # Disable detection of different BLAS/LAPACK implementations
        --with-blas=blas
        --with-lapack=lapack

        # Enable sparse algorithms for solvers & eigenvalue problems
        --with-amd
        --with-arpack
        --with-camd
        --with-ccolamd
        --with-cholmod
        --with-colamd
        --with-cxsparse
        --with-qrupdate
        --with-suitesparseconfig
        --with-umfpack

        # Unwritten
        --without-klu
        --without-spqr
        --without-sundials_ida
        --without-sundials_nvecserial
        --without-sundials_sunlinsolklu

        # Untested
        --without-openssl
        --without-qhull_r

        # Bundled freefont fonts are in otf format
        --without-system-freefont

        $(option_enable doc docs)
        $(option_enable openmp)
        $(option_with curl)
        $(option_with gui qscintilla)
        $(option_with gui qt 5)
        $(option_with opengl)
        $(option_with opengl x)
        $(option_with opengl fltk)
        $(option_with portaudio)
        $(option_with sndfile)
    )

    if option providers:graphicsmagick; then
        myconf+=( --with-magick=GraphicsMagick )
#    elif option providers:imagemagick; then
#        myconf+=( --with-magick=ImageMagick )
    else
        myconf+=( --without-magick )
    fi

    econf "${myconf[@]}"
}

octave_src_install() {
    default

    edo dodoc $(find -name 'refcard-*.pdf' -o -name '*octave.pdf')
    edo pushd "${IMAGE}" >/dev/null
    edo keepdir $(find usr/{share,$(exhost --target)/lib{,exec}}/octave/ -type d -empty -printf '/%p\n')
    edo popd >/dev/null

    ! option gui && edo rm -rf "${IMAGE}"/usr/share/{applications,icons,metainfo}
}

octave_pkg_postinst() {
    option gui && freedesktop-desktop_pkg_postinst
    option gui && gtk-icon-cache_pkg_postinst
}

octave_pkg_postrm() {
    option gui && freedesktop-desktop_pkg_postrm
    option gui && gtk-icon-cache_pkg_postrm
}

