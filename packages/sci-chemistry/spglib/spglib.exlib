# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=spglib tag=v${PV} ]
require cmake

SUMMARY="A library for finding and handling crystal symmetries written in C"
DESCRIPTION="
* Find symmetry operations
* Identify space-group type
* Wyckoff position assignment
* Refine crystal structure
* Find a primitive cell
* Search irreducible k-points
"

HOMEPAGE+=" https://spglib.github.io/spglib/"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS="
    fortran
    openmp [[ description = [ Enable parallel processing using OpenMP ] ]]
"

DEPENDENCIES="
    build+run:
        fortran? ( sys-libs/libgfortran:= )
        openmp? ( sys-libs/libgomp:= )

"

# Test aren't included with the build and they want to download gtest
RESTRICT="test"

UPSTREAM_CHANGELOG="https://github.com/atztogo/spglib/blob/master/ChangeLog"

CMAKE_SRC_CONFIGURE_OPTION_USES=( 'openmp OMP' )
CMAKE_SRC_CONFIGURE_OPTION_WITHS=( 'fortran Fortran' )

