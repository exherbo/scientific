# Copyright 2015 Sergey Kvachonok <ravenexp@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ none ] need_libtool=false ]
require github [ user=steveicarus tag=v$(ever replace_all _) ]

SUMMARY="Icarus Verilog is a Verilog simulation and synthesis tool"
DESCRIPTION="
Icarus Verilog operates as a compiler, compiling source code
written in Verilog (IEEE-1364) into some target format.
"

HOMEPAGE="http://${PN}.icarus.com/"

SLOT="0"
LICENCES="GPL-2"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-util/gperf
        sys-devel/bison
        sys-devel/flex
"

export_exlib_phases src_prepare src_install

iverilog_src_prepare() {
    default

    eautoconf
}

# Custom Makefile.in tries to support installing to ${DESTDIR}, but...
# If up to date versions of the files to be installed are already
# present in ${prefix}/ they won't get installed into ${DESTDIR}
# breaking every other install image and all pbins.
iverilog_src_install() {

    # Prepend ${DESTDIR} to ${prefix} and friends just for 'make install'.
    # This prevents make from seeing older files in ${prefix}/ and
    # all installed files still end up under ${IMAGE}/.
    emake -j1 DESTDIR="" \
              prefix="${IMAGE}"/usr/$(exhost --target) \
              bindir="${IMAGE}"/usr/$(exhost --target)/bin \
              libdir="${IMAGE}"/usr/$(exhost --target)/lib \
              mandir="${IMAGE}"/usr/share/man \
              vpidir="${IMAGE}"/usr/$(exhost --target)/lib/ivl \
              install

    emagicdocs
}

